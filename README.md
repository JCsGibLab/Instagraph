# Instagraph
[![License](https://img.shields.io/badge/license-GPLv3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.en.html)
[![GitHub release](https://img.shields.io/github/release/turanmahmudov/Instagraph.svg)](https://gitlab.com/rubencarneiro/Instagraph)
[![GitHub issues](https://img.shields.io/github/issues/turanmahmudov/Instagraph.svg)](https://gitlab.com/rubencarneiro/Instagraph/-/issues)

Unofficial Native Instagram Client

## Installation

### Click for Ubuntu Phones

- Search "Instagraph" in the OpenStore and install the app
- Direct download ".click" package from the OpenStore

<a href="https://open-store.io/app/instagraph-rubencarneiro" rel="Openstore">![Foo](https://open-store.io/badges/en_US.png)</a>

### Snap

`snap install instagraph`

`snap connect instagraph:camera :camera`

`snap connect instagraph:alsa :alsa`

`snap connect instagraph:pulseaudio :pulseaudio`

`snap connect instagraph:opengl :opengl`

## Usage

## Missing Features
- Playback Videos

## Contributing
- All translators
- You that donate

## Translations
POEditor [https://poeditor.com/join/project/wZiqQyM7ZS](https://poeditor.com/join/project?hash=fO5l71L1Ig)

## Credits
- Creator & Developer: Turan Mahmudov <[turan.mahmudov@gmail.com](mailto:turan.mahmudov@gmail.com)>
- Maintainer: Rúben Carneiro <[rubencarneiro01@gmail.com](malito:rubencarneiro01@gmail.com)>
- Icons: Kevin Feyder <[kevinfeyder@gmail.com](mailto:kevinfeyder@gmail.com)>

## Libraries
- [instantfx](http://launchpad.net/instantfx): A photo filter application for Ubuntu Devices
- [neochapay/Prostogram](https://github.com/neochapay/Prostogram): An unoffical Instagram client for Sailfish
- [mitmproxy/mitmproxy](https://github.com/mitmproxy/mitmproxy): An interactive SSL-capable intercepting HTTP proxy

## Support
Reach out to me at one of the following places!

- Twitter at <a href="https://twitter.com/rubenlcarneiro" target="_blank">`@rubenlcarneiro`</a>
- Telegram at <a href="https://t.me/rubencarneiro" target="_blank">`@rubencarneiro`</a>

## Donations
Donate me on [PayPal](https://paypal.me/rubencarneiro?locale.x=pt_PT)

## License
The app is open source and licensed under GNU General Public License v3.0 (http://www.gnu.org/licenses/gpl-3.0.en.html).
